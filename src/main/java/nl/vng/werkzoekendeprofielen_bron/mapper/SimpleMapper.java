package nl.vng.werkzoekendeprofielen_bron.mapper;

import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import org.mapstruct.Mapper;

/**
 * Interface from which Mapstruct generates a mapper from Werkzoekende to MPWerkzoekendeMatch.
 */
@Mapper(
        componentModel = "spring"
)
public interface SimpleMapper {

    MPWerkzoekendeMatch werkzoekendeToMPWerkzoekendeMatch(Werkzoekende werkzoekende);
}
