package nl.vng.werkzoekendeprofielen_bron.serializer;

import nl.vng.werkzoekendeprofielen_bron.entity.Telefoonnummer;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class ValueSerializerTelefoonnummer extends JsonSerializer<Telefoonnummer> {

    @Override
    public void serialize(final Telefoonnummer telefoonnummer, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(telefoonnummer.getTelefoonnummer());
    }
}
