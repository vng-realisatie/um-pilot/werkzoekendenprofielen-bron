package nl.vng.werkzoekendeprofielen_bron.controller;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.InlineResponse200;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchCertWithOinVumProviderException;
import nl.vng.werkzoekendeprofielen_bron.exception.WerkzoekendeNotFoundException;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.VumService;
import nl.vng.werkzoekendeprofielen_bron.util.Error;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Optional;

@Validated
@Hidden
@Controller
@Slf4j
@CrossOrigin()
public class VumController {

	public static final String X_VUM_BERICHT_VERSIE = "X-VUM-berichtVersie";
    public static final String X_VUM_FROM_PARTY = "X-VUM-fromParty";
    public static final String X_VUM_VIA_PARTY = "X-VUM-viaParty";
    public static final String X_VUM_TO_PARTY = "X-VUM-toParty";
    public static final String X_VUM_SUWIPARTY = "X-VUM-SUWIparty";
    public static final String X_FORWARDED_FOR = "X-Forwarded-For";

    private final VumService vumService;

    private final ElkService elkService;
    
    private final ConfigProperties config;

    @Autowired
    public VumController(final VumService vum, final ElkService elk, final ConfigProperties configProperties) {
        this.vumService = vum;
        this.elkService = elk;
        this.config = configProperties;
    }

    /**
     * GET /werkzoekendeProfielen/{idWerkzoekende} : Vraag detailprofielen op bij opgegeven idWerkzoekende.
     * Vraag detailprofielen op bij een opgegeven idWerkzoekende
     *
     * @param idWerkzoekende    (required)
     * @param xVUMBerichtVersie (required)
     * @param xVUMFromParty     (required)
     * @param xVUMToParty       (required)
     * @param xVUMSUWIparty     (required)
     * @param xVUMViaParty      (optional)
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Too Many Requests, limiet is overschreden voor deze uitvraag (status code 429)
     * or Internal Server Error (status code 500)
     * or Service Unavailable (status code 503)
     */
    @Operation(summary = "Vraag detailprofielen op bij opgegeven idWerkzoekende", description = "Vraag detailprofielen op bij een opgegeven idWerkzoekende", tags = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = Werkzoekende.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "401", description = "not authorized"),
            @ApiResponse(responseCode = "429", description = "Too Many Requests, limiet is overschreden voor deze uitvraag", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "503", description = "Service Unavailable", content = @Content(schema = @Schema(implementation = Error.class)))})
    @GetMapping(
            value = "/werkzoekendeProfielen/{idWerkzoekende}",
            produces = {"application/json"}
    )
    public ResponseEntity<Werkzoekende> getWerkzoekendeProfiel(@Size(max = 200) @Parameter(required = true) @PathVariable("idWerkzoekende") final String idWerkzoekende,
                                                               @Parameter(required = true) @RequestHeader(value = X_VUM_BERICHT_VERSIE, required = true) final String xVUMBerichtVersie,
                                                               @Pattern(regexp="^\\d{20}$") @Parameter(required = true) @RequestHeader(value = X_VUM_FROM_PARTY, required = true) final String xVUMFromParty,
                                                               @Pattern(regexp="^\\d{20}$") @Parameter(required = true) @RequestHeader(value = X_VUM_TO_PARTY, required = true) final String xVUMToParty,
                                                               @Parameter(required = true) @RequestHeader(value = X_VUM_SUWIPARTY, required = true) final Boolean xVUMSUWIparty,
                                                               @Parameter(required = true) @RequestHeader(value = X_FORWARDED_FOR, required = true) final String xForwardedFor,
                                                               @Pattern(regexp="^\\d{20}$") @Parameter() @RequestHeader(value = X_VUM_VIA_PARTY, required = false) final String xVUMViaParty) {
        log.info("Aanvraag detailprofiel van ID: " + idWerkzoekende + " door OIN: " + xVUMFromParty + " aan OIN:" + xVUMToParty);
        
        validateXForwardedForHeader(xForwardedFor);
        
        Optional<Werkzoekende> werkzoekendeOptional = vumService.findById(idWerkzoekende, xVUMToParty);
        if (werkzoekendeOptional.isPresent()) {
            log.info("Succesvolle aanvraag detailprofiel van ID: " + idWerkzoekende + " door OIN: " + xVUMFromParty + " aan OIN:" + xVUMToParty);
            elkService.handleRequest(
                    werkzoekendeOptional.get(),
                    xVUMToParty,
                    xVUMFromParty,
                    "GET /werkzoekendeProfielen/{idWerkzoekende}",
                    "Vraag detailprofielen op bij opgegeven idWerkzoekende",
                    "/werkzoekendeProfielen/" + idWerkzoekende

            );

            HttpHeaders headers = new HttpHeaders();
            headers.set(X_VUM_BERICHT_VERSIE, "1.0.0");
            headers.set(X_VUM_FROM_PARTY, xVUMToParty);
            if (!"".equals(xVUMViaParty)) {
            	headers.set(X_VUM_VIA_PARTY, xVUMViaParty); //ViaParty is gelijk aan bemiddelaar provider uit request
            }
            headers.set(X_VUM_TO_PARTY, xVUMFromParty); //ToParty is gelijk aan bemiddelaar uit request
            return new ResponseEntity<>(werkzoekendeOptional.get(), headers, HttpStatus.OK);
        } else {
            log.error("Mislukte aanvraag detailprofiel van ID: " + idWerkzoekende + " door OIN: " + xVUMFromParty + " aan OIN:" + xVUMToParty);
            elkService.handleRequest(
                    new WerkzoekendeNotFoundException("ID niet gevonden"),
                    xVUMToParty,
                    xVUMFromParty,
                    "GET /werkzoekendeProfielen/{idWerkzoekende}",
                    "Vraag detailprofielen op bij opgegeven idWerkzoekende",
                    "/werkzoekendeProfielen/" + idWerkzoekende

            );
            throw new WerkzoekendeNotFoundException("ID niet gevonden");
        }
    }


    /**
     * POST /werkzoekendeProfielen/matches : Zoekopdracht voor MatchingProfielen Werkzoekende.
     * Zoekopdracht om matches voor Werkzoekende MatchingProfielen op te vragen
     *
     * @param xVUMBerichtVersie (required)
     * @param xVUMFromParty     (required)
     * @param xVUMToParty       (required)
     * @param xVUMSUWIparty     (required)
     * @param matchesRequest    (required)
     * @param xVUMViaParty      (optional)
     * @param request           (optional)
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Too Many Requests (status code 429)
     * or Internal Server Error (status code 500)
     * or Service Unavailable (status code 503)
     */
    @Operation(summary = "Zoekopdracht voor MatchingProfielen Werkzoekende", description = "Zoekopdracht om matches voor Werkzoekende MatchingProfielen op te vragen", tags = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = InlineResponse200.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "401", description = "not authorized"),
            @ApiResponse(responseCode = "429", description = "Too Many Requests", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "503", description = "Service Unavailable", content = @Content(schema = @Schema(implementation = Error.class)))})
    @PostMapping(
            value = "/werkzoekendeProfielen/matches",
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<InlineResponse200> matchesWerkzoekendeProfielen(@Parameter(required = true) @RequestHeader(value = X_VUM_BERICHT_VERSIE, required = true) final String xVUMBerichtVersie,
    		                                                              @Pattern(regexp="^\\d{20}$") @Parameter(required = true) @RequestHeader(value = X_VUM_FROM_PARTY, required = true) final String xVUMFromParty,
    		                                                              @Pattern(regexp="^\\d{20}$") @Parameter(required = true) @RequestHeader(value = X_VUM_TO_PARTY, required = true) final String xVUMToParty,
                                                                          @Parameter(required = true) @RequestHeader(value = X_VUM_SUWIPARTY, required = true) final Boolean xVUMSUWIparty,
                                                                          @Parameter(required = true) @Valid @RequestBody(required = true) final WerkzoekendeProfielMatchesRequest matchesRequest,
    		                                                              @Parameter(required = true) @RequestHeader(value = X_FORWARDED_FOR, required = true) final String xForwardedFor,
                                                                          @Pattern(regexp="^\\d{20}$") @Parameter() @RequestHeader(value = X_VUM_VIA_PARTY, required = false) final String xVUMViaParty, final  HttpServletRequest request) {

        log.info("Aanvraag voor matchingprofielen werkzoekenden door OIN: " + xVUMFromParty + " aan OIN:" + xVUMToParty);
        
        validateXForwardedForHeader(xForwardedFor);
        
        elkService.handleRequest(
                matchesRequest,
                xVUMToParty,
                xVUMFromParty,
                "POST /werkzoekendeProfielen/matches",
                "Zoekopdracht voor MatchingProfielen Werkzoekende",
                "/werkzoekendeProfielen/matches"

        );
        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> matches = vumService.match(matchesRequest, xVUMToParty);
        log.info("Succesvolle aanvraag voor matchingprofielen werkzoekenden door OIN: " + xVUMFromParty + " aan OIN:" + xVUMToParty);
        elkService.handleRequest(
                matches,
                xVUMToParty,
                xVUMFromParty,
                "POST /werkzoekendeProfielen/matches",
                "Zoekopdracht voor MatchingProfielen Werkzoekende",
                "/werkzoekendeProfielen/matches"

        );
        HttpHeaders headers = new HttpHeaders();
        headers.set(X_VUM_BERICHT_VERSIE, "1.0.0");
        headers.set(X_VUM_FROM_PARTY, xVUMToParty);
        if (!"".equals(xVUMViaParty)) {
        	headers.set(X_VUM_VIA_PARTY, xVUMViaParty); //ViaParty is gelijk aan bemiddelaar provider uit request
        }
        headers.set(X_VUM_TO_PARTY, xVUMFromParty); //ToParty is gelijk aan bemiddelaar uit request
        return new ResponseEntity<>(new InlineResponse200(matches.getLeft(), matches.getRight()), headers, HttpStatus.OK);
    }
    
    /**
     * Indien xForwardedFor geen VUM provider OIN gooi exceptie.
     * Gebruik de VUM provider OIN zonder voorloop nullen
     * 
     * @param xForwardedFor
     */
    private void validateXForwardedForHeader(String xForwardedFor) {
    	String vumProviderOinTrimmed = config.getVumProviderOin().replaceFirst("^0+(?!$)", "");
    	if (!xForwardedFor.contains(vumProviderOinTrimmed)) {
        	log.info(X_FORWARDED_FOR + "='"+ xForwardedFor + "'");
            throw new MismatchCertWithOinVumProviderException(); 
        }		
	}

}
