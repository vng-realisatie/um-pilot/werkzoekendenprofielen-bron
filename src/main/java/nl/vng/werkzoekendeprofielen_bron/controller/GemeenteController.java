package nl.vng.werkzoekendeprofielen_bron.controller;

import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.GemeenteService;
import nl.vng.werkzoekendeprofielen_bron.util.Error;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@Tag(name = "werkzoekendeProfielen")
@Slf4j
@CrossOrigin()
public class GemeenteController {

    private final GemeenteService service;

    private final ElkService elkService;

    @Autowired
    public GemeenteController(final GemeenteService gemeenteService, final ElkService elk) {
        this.service = gemeenteService;
        this.elkService = elk;
    }

    /**
     * POST /werkzoekende/lijst/{oin} : Endpoint to create several Werkzoekende from a list.
     *
     * @param werkzoekenden list of Werkzoekende.
     * @param oin           identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Internal Server Error (status code 500)
     */
    @Operation(summary = "Post werkzoekenden", description = "Post werkzoekenden to a given OIN", tags = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Werkzoekende.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "401", description = "not authenticated", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "403", description = "Geen authorizatie", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = Error.class)))})
    @PostMapping(
            value = "werkzoekende/lijst/{oin}",
            produces = {"application/json"}
    )
    public List<Werkzoekende> createWerkzoekendeList(@Valid @RequestBody final List<Werkzoekende> werkzoekenden, @PathVariable final String oin) {
        log.info("Upload van profielen door OIN: " + oin);
        String oinUser = getOinUser();
        if (oinUser.equals(oin)) {
            log.info("Succesvolle upload van profielen door OIN: " + oinUser);
            elkService.handleRequest(
                    werkzoekenden,
                    oin,
                    oinUser,
                    "POST /werkzoekende/lijst/{oin}",
                    "Endpoint to create several Werkzoekende from a list",
                    "/werkzoekende/lijst/" + oin
            );
            return service.saveAll(werkzoekenden, oin);
        } else {
            log.info("Mislukte upload van profielen door OIN: " + oinUser + "Er is geprobeerd te uploaden voor OIN:" + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    oinUser,
                    "POST /werkzoekende/lijst/{oin}",
                    "Endpoint to create several Werkzoekende from a list",
                    "/werkzoekende/lijst/" + oin
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }


    /**
     * GET /werkzoekende/lijst/{oin} : Endpoint to retrieve all Werkzoekende from the DB.
     *
     * @param oin identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Internal Server Error (status code 500)
     */
    @Operation(summary = "Get werkzoekenden", description = "Get werkzoekenden from DB", tags = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Werkzoekende.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "401", description = "not authenticated", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "403", description = "Geen authorizatie", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = Error.class)))})

    @GetMapping(
            value = "werkzoekende/lijst/{oin}",
            produces = {"application/json"}
    )
    public List<Werkzoekende> getAll(@PathVariable final String oin) {
        String oinUser = getOinUser();
        if (oinUser.equals(oin)) {
            List<Werkzoekende> werkzoekenden = service.findAll(oin);
            elkService.handleRequest(
                    werkzoekenden,
                    oin,
                    oinUser,
                    "GET /werkzoekende/lijst/{oin}",
                    "Endpoint to retrieve all Werkzoekende from the DB",
                    "/werkzoekende/lijst/" + oin
            );
            return werkzoekenden;
        } else {
            log.info("Mislukte GET profielen door OIN: " + oinUser + "Er is geprobeerd GET voor OIN:" + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    oinUser,
                    "GET /werkzoekende/lijst/{oin}",
                    "Endpoint to retrieve all Werkzoekende from the DB",
                    "/werkzoekende/lijst/" + oin
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }

    }


    /**
     * Utility method to extract OIN from the jwt token given by the user.
     *
     * @return OIN of the user or empty string if no OIN given.
     */
    private String getOinUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String oinUser = ((JwtAuthenticationToken) authentication).getToken().getClaim("oin");
        return oinUser == null ?  "" : oinUser;
    }
}
