package nl.vng.werkzoekendeprofielen_bron.repository;

import nl.vng.werkzoekendeprofielen_bron.dto.ElkEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends ElasticsearchRepository<ElkEntity, String> {
}
