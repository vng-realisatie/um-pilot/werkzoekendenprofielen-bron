package nl.vng.werkzoekendeprofielen_bron.repository;

import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface WerkzoekendeRepository extends JpaRepository<Werkzoekende, String>, JpaSpecificationExecutor<Werkzoekende> {

    Optional<Werkzoekende> findByIdWerkzoekendeAndOin(String idWerkzoekende, String oin);

    List<Werkzoekende> findAllByOin(String oin);

    @Transactional
    List<Werkzoekende> deleteByOin(String oin);
}
