package nl.vng.werkzoekendeprofielen_bron.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    public static final String HTTPS_EXAMPLEURL_COM = "https://exampleurl.com";
    public static final String TITLE = "UM API WerkzoekendeProfielen tussen UM en gemeenten";
    public static final String VERSION = "0.8.2";
    public static final String DESCRIPTION = "Contract tbv interactie van gemeenten met UM voor WerkzoekendenProfielen";
    public static final String VNG = "VNG";
    public static final String URL = "https://www.vngrealisatie.nl/";
    public static final String EMAIL = "realisatie@vng.nl";
    @Bean
    public OpenAPI customOpenAPI() {

        return new OpenAPI()
                .addServersItem(new Server()
                        .url(HTTPS_EXAMPLEURL_COM))
                .components(new Components())
                .info(new Info()
                        .title(TITLE)
                        .version(VERSION)
                        .description(DESCRIPTION)
                        .contact(new Contact()
                                .name(VNG)
                                .url(URL)
                                .email(EMAIL)));


    }
}
