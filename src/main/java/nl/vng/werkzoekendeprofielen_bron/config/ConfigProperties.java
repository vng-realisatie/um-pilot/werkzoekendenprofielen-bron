package nl.vng.werkzoekendeprofielen_bron.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * Class that obtains environment variables from application.properties starting with prefix vng.
 */
@Configuration
@ConfigurationProperties(prefix = "vng")
@Getter
@Setter
public class ConfigProperties {
    private String vumProviderOin;
    private String[] setAllowedOrigins;
    private String[] setAllowedMethods;
    private String[] setAllowedHeaders;
    private int maxAmountResponse;
}
