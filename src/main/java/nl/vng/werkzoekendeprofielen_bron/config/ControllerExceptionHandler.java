package nl.vng.werkzoekendeprofielen_bron.config;

import nl.vng.werkzoekendeprofielen_bron.exception.WerkzoekendeNotFoundException;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchCertWithOinVumProviderException;
import nl.vng.werkzoekendeprofielen_bron.util.Error;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    public static final ImmutablePair<String, String> ONGELDIGE_AANROEP = ImmutablePair.of("400.01", "Ongeldige aanroep");
    public static final ImmutablePair<String, String> ONVOLLEDIGE_AANROEP = ImmutablePair.of("400.02", "Onvolledige aanroep");
    public static final ImmutablePair<String, String> NIET_GEAUTORISEERDE_AANROEP = ImmutablePair.of("403.01", "Geen authorizatie");
    @ResponseBody
    @ExceptionHandler(MissingRequestHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleMissingRequestHeaderException(final MissingRequestHeaderException exception) {
        return new Error(ONVOLLEDIGE_AANROEP.getLeft(), ONVOLLEDIGE_AANROEP.getRight(), exception.getLocalizedMessage());
    }

    @ResponseBody
    @ExceptionHandler(MismatchRequestWithOinBemiddelaarException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Error handleMismatchRequestWithOinBemiddelaarException(final MismatchRequestWithOinBemiddelaarException exception) {
        //TODO: confirm code
        return new Error(NIET_GEAUTORISEERDE_AANROEP.getLeft(), NIET_GEAUTORISEERDE_AANROEP.getRight(), exception.getLocalizedMessage());
    }
    
    @ResponseBody
    @ExceptionHandler(MismatchCertWithOinVumProviderException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Error handleMismatchCertWithOinVumProviderException(MismatchCertWithOinVumProviderException exception) {
        return new Error(NIET_GEAUTORISEERDE_AANROEP.getLeft(), NIET_GEAUTORISEERDE_AANROEP.getRight(), exception.getLocalizedMessage());
    }

    @ResponseBody
    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleNumberFormatException(final NumberFormatException exception) {
        return new Error(ONGELDIGE_AANROEP.getLeft(), ONGELDIGE_AANROEP.getRight(), exception.getLocalizedMessage());
    }


    @ResponseBody
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleMethodArgumentTypeMismatchException(final MethodArgumentTypeMismatchException exception) {
        return new Error(ONGELDIGE_AANROEP.getLeft(), ONGELDIGE_AANROEP.getRight(), exception.getLocalizedMessage());
    }


    @ResponseBody
    @ExceptionHandler(WerkzoekendeNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleNoSuchElementFoundException(final WerkzoekendeNotFoundException exception) {
        return new Error(ONGELDIGE_AANROEP.getLeft(), ONGELDIGE_AANROEP.getRight(), exception.getReason());
    }

    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleConstraintViolationException(final ConstraintViolationException exception) {
        return new Error(ONGELDIGE_AANROEP.getLeft(), ONGELDIGE_AANROEP.getRight(), exception.getLocalizedMessage());
    }


    /**
     * Handles exceptions whenever an entity is not valid.
     *
     * @param ex        the exception
     * @param headers   the headers
     * @param status    the http Status
     * @param request   the request
     *
     * @return the ResponseEntity
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        Error response = new Error(ONGELDIGE_AANROEP.getLeft(), ONGELDIGE_AANROEP.getRight(), errors.toString());

        return super.handleExceptionInternal(ex, response, headers, status, request);
    }

    /**
     * Handles exceptions whenever incoming JSON is not able to be read.
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

        Error response = new Error(ONVOLLEDIGE_AANROEP.getLeft(), ONVOLLEDIGE_AANROEP.getRight(), ex.getLocalizedMessage());

        return super.handleExceptionInternal(ex, response, headers, status, request);

    }
}
