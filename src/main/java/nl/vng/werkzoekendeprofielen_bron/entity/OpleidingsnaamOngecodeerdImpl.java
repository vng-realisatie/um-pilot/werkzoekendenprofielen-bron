package nl.vng.werkzoekendeprofielen_bron.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@Embeddable
public class OpleidingsnaamOngecodeerdImpl {
    @Size(max = 120)
    @Schema(description = "De omschrijving van de OPLEIDING.")
    private String naamOpleidingOngecodeerd;

    @Size(max = 2000)
    @Schema(description = "De eigen omschrijving van de opleiding.")
    private String omschrijvingOpleiding;
}
