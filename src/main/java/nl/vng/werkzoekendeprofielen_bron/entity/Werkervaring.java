package nl.vng.werkzoekendeprofielen_bron.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Werkervaring
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@JsonInclude(Include.NON_NULL)
public class Werkervaring extends MPWerkervaring {

    @Size(max = 2000)
    @Schema(description = "De toelichting op de WERKERVARING.")
    private String toelichtingWerkervaring;


}
