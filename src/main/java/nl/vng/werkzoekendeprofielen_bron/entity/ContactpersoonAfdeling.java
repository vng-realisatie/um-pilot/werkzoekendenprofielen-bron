package nl.vng.werkzoekendeprofielen_bron.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * ContactpersoonAfdeling
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@Embeddable
public class ContactpersoonAfdeling {

    @Size(max = 35)
    @Schema(description = "De naam van de CONTACTPERSOON/-AFDELING")
    private String naamContactpersoonAfdeling;

    @Embedded
    @Valid
    private Telefoonnummer telefoonnummer;

    @Embedded
    @Valid
    private Emailadres emailadres;


}
