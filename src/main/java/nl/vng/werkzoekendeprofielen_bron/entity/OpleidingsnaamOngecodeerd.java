package nl.vng.werkzoekendeprofielen_bron.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.Valid;

/**
 * OpleidingsnaamOngecodeerd
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OpleidingsnaamOngecodeerd extends Opleidingsnaam {

   @Valid
   @Embedded
   private OpleidingsnaamOngecodeerdImpl opleidingsnaamOngecodeerd;

}
