package nl.vng.werkzoekendeprofielen_bron.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WerkzoekendeCompositeId implements Serializable {

    private String idWerkzoekende;
    private String oin;

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        WerkzoekendeCompositeId werkzoekendeCompositeId = (WerkzoekendeCompositeId) o;
        return idWerkzoekende.equals(werkzoekendeCompositeId.idWerkzoekende) &&
                oin.equals(werkzoekendeCompositeId.oin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idWerkzoekende, oin);
    }
}
