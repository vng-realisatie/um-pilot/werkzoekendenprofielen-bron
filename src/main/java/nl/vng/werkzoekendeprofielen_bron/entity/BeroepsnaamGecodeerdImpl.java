package nl.vng.werkzoekendeprofielen_bron.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

/**
 * BeroepsnaamOngecodeerd
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@Embeddable
public class BeroepsnaamGecodeerdImpl {
    @Size(max = 10)
    @Schema(description = "De unieke code van de beroepsnaam uit het BO&C register.")
    private String codeBeroepsnaam;

    @Size(max = 120)
    @Schema(description = "De naam van het BEROEP.")
    private String omschrijvingBeroepsnaam;
}
