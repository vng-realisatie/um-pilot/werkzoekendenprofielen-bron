package nl.vng.werkzoekendeprofielen_bron.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.Valid;

/**
 * BeroepsnaamOngecodeerd
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BeroepsnaamOngecodeerd extends Beroepsnaam {

    @Valid
    @Embedded
    private BeroepsnaamOngecodeerdImpl beroepsnaamOngecodeerd;
}
