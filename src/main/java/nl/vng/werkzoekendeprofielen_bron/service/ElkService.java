package nl.vng.werkzoekendeprofielen_bron.service;

import nl.vng.werkzoekendeprofielen_bron.dto.ElkEntity;
import nl.vng.werkzoekendeprofielen_bron.repository.RequestRepository;
import org.springframework.stereotype.Service;

@Service
public class ElkService {

    private final RequestRepository requestRepository;

    public ElkService(final RequestRepository repository) {
        this.requestRepository = repository;
    }

    /**
     * Save the match request in ElasticSearch.
     *
     * @param request            Request to be saved
     * @param toOin              OIN of the recipient
     * @param fromOin            OIN of the sender
     * @param requestSignature   Request signature
     * @param requestDescription Request description
     * @param requestPath        Request path
     */
    public void handleRequest(
            final Object request,
            final String toOin,
            final String fromOin,
            final String requestSignature,
            final String requestDescription,
            final String requestPath
    ) {
        requestRepository.save(new ElkEntity(request, toOin, fromOin, requestSignature, requestDescription, requestPath
        ));
    }
}


