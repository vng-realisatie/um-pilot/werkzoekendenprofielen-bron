package nl.vng.werkzoekendeprofielen_bron.service;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.mapper.SimpleMapper;
import nl.vng.werkzoekendeprofielen_bron.repository.WerkzoekendeRepository;
import nl.vng.werkzoekendeprofielen_bron.repository.WerkzoekendeSpecifications;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class VumService {

    private final WerkzoekendeRepository werkzoekendeRepository;

    private final SimpleMapper mapper;

    private final int maxAmount;

    @Autowired
    public VumService(final WerkzoekendeRepository repository, final SimpleMapper simpleMapper, final ConfigProperties properties) {
        this.werkzoekendeRepository = repository;
        this.mapper = simpleMapper;
        maxAmount = properties.getMaxAmountResponse();
    }

    /**
     * Find Werkzoekende with given ID in a given Organisation.
     *
     * @param idWerkzoekende id of Werkzoekende to be found.
     * @param oin id of the organisation
     * @return Werkzoekende with given ID.
     */
    public Optional<Werkzoekende> findById(final String idWerkzoekende, final String oin) {
        return werkzoekendeRepository.findByIdWerkzoekendeAndOin(idWerkzoekende, oin);
    }

    /**
     * Find all Werkzoekende who match the given request.
     *
     * @param matchesRequest the criteria used to filter the Werkzoekende from the DB.
     * @param toOin          the OIN of the municipality.
     * @return Pair of boolean and matching profiles. The boolean is true if the matching profiles exceed the MAX_AMOUNT.
     * Else the boolean is false.
     */
    public ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> match(final WerkzoekendeProfielMatchesRequest matchesRequest, final String toOin) {
        // Get the specifications based on the matchesRequest.
        Specification<Werkzoekende> werkzoekendeSpecifications = WerkzoekendeSpecifications
                .createWerkZoekendeSpecification(matchesRequest.getVraagObject(), toOin);
        // Search the DB with these specifications.
        List<Werkzoekende> werkzoekenden = werkzoekendeRepository.findAll(werkzoekendeSpecifications);
        // Shuffle and limit werkzoekenden.
        List<Werkzoekende> limitedWerkzoekenden = pickRandom(werkzoekenden, maxAmount);
        // use mapper to map Werkzoekende to MPWerkzoekendeMatch.
        List<MPWerkzoekendeMatch> mpWerkzoekendenmatch = limitedWerkzoekenden
                .stream()
                .map(mapper::werkzoekendeToMPWerkzoekendeMatch)
                .collect(Collectors.toList());
        return new ImmutablePair<>(werkzoekenden.size() >= maxAmount, new WerkzoekendeMatchingProfielen(mpWerkzoekendenmatch));
    }

    /**
     * Util function to randomly pick max amount of werkzoekende coming from the DB.
     *
     * @param werkzoekendeList List to be limited.
     * @param max Max amount.
     * @return Shuffled and limited list of werkzoekende.
     */
    public List<Werkzoekende> pickRandom(final List<Werkzoekende> werkzoekendeList, final int max) {
        // Already less than max amount.
        if (werkzoekendeList.size() <= max) {
            return werkzoekendeList;
        }
        //Shuffle in O(n).
        Collections.shuffle(werkzoekendeList);
        // Pick max amount from new list.
        return werkzoekendeList.stream().limit(max).collect(Collectors.toList());

    }
}
