package nl.vng.werkzoekendeprofielen_bron.service;

import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.repository.WerkzoekendeRepository;
import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class GemeenteService {

    private final WerkzoekendeRepository repository;

    @Autowired
    public GemeenteService(final WerkzoekendeRepository repo) {
        this.repository = repo;
    }

    /**
     * Save all Werkzoekende from the list into the DB.
     *
     * @param werkzoekenden list of Werkzoekende to be saved in the DB.
     * @param oin           identifier of municipality of Werkzoekende.
     * @return List of Werkzoekende entities after being saved in the DB.
     */
    public List<Werkzoekende> saveAll(final List<Werkzoekende> werkzoekenden, final String oin) {
        // first delete all users
        repository.deleteByOin(oin);

        werkzoekenden.forEach(x -> {
            //Add OIN to each werkzoekende before saving to DB
            x.setOin(oin);
            //Hash ID before saving to DB
            x.setIdWerkzoekende(hashId(x.getIdWerkzoekende()));
        });
        return repository.saveAll(werkzoekenden);
    }

    private String hashId(final String idWerkzoekende) {
        Keccak.Digest256 digest256 = new Keccak.Digest256();
        byte[] hashbytes = digest256.digest(
                idWerkzoekende.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encode(hashbytes));
    }


    /**
     * Retrieve all Werkzoekende for a particular OIN.
     *
     * @param oin de identifier van de organisatie
     * @return the Werkzoekenden
     */
    public List<Werkzoekende> findAll(final String oin) {
        return repository.findAllByOin(oin);
    }
}
