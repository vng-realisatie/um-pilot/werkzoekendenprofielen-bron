package nl.vng.werkzoekendeprofielen_bron.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class MismatchCertWithOinVumProviderException extends ResponseStatusException {

    public MismatchCertWithOinVumProviderException() {
        super(HttpStatus.FORBIDDEN, "Het OIN is niet gelijk aan het OIN van de VUM Voorziening (HTTPS Certificaat)");
    }
}
