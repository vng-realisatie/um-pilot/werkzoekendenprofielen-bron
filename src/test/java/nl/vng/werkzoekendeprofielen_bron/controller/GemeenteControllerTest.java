package nl.vng.werkzoekendeprofielen_bron.controller;

import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.GemeenteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the GemeenteController
 */
@ExtendWith(MockitoExtension.class)
class GemeenteControllerTest {

    public static final String OIN = "OIN";
    @Mock
    private ElkService elkService;

    @Mock
    private GemeenteService gemeenteService;

    @Mock
    private SecurityContext securityContext;

    private JwtAuthenticationToken authentication;

    @InjectMocks
    private GemeenteController gemeenteController;

    @BeforeEach
    private void setUp() {
        authentication = new JwtAuthenticationToken(Jwt
                .withTokenValue("oin")
                .claim("oin", OIN)
                .header("", "")
                .build());
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void shouldAddWerkzoekendeList() {

        List<Werkzoekende> werkzoekenden = Arrays.asList(new Werkzoekende());
        gemeenteController.createWerkzoekendeList(werkzoekenden, OIN);

        String requestSignature = "POST /werkzoekende/lijst/{oin}";
        String requestDescription = "Endpoint to create several Werkzoekende from a list";
        String requestPath = "/werkzoekende/lijst/OIN";

        verify(elkService).handleRequest(werkzoekenden, OIN, OIN, requestSignature, requestDescription, requestPath);
        verify(gemeenteService).saveAll(werkzoekenden, OIN);
    }

    @Test
    void shouldFindAll() {

        List<Werkzoekende> werkzoekenden = Arrays.asList(new Werkzoekende());
        when(gemeenteService.findAll(OIN)).thenReturn(werkzoekenden);

        List<Werkzoekende> all = gemeenteController.getAll(OIN);

        String requestSignature = "GET /werkzoekende/lijst/{oin}";
        String requestDescription = "Endpoint to retrieve all Werkzoekende from the DB";
        String requestPath = "/werkzoekende/lijst/OIN";

        verify(gemeenteService).findAll(OIN);
        verify(elkService).handleRequest(werkzoekenden, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertEquals(werkzoekenden, all);
    }

    @Test()
    void shouldFailBuildingOinUserGetAll() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.getAll("WRONG"), "WrongOinException is verwacht");
    }

    @Test()
    void shouldFailBuildingOinUserCreate() {

        List<Werkzoekende> werkzoekenden = Arrays.asList(new Werkzoekende());

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.createWerkzoekendeList(werkzoekenden, "WRONG"), "WrongOinException is verwacht");
    }
}