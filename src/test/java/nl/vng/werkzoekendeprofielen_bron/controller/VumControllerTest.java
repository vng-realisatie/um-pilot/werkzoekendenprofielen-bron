package nl.vng.werkzoekendeprofielen_bron.controller;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchCertWithOinVumProviderException;
import nl.vng.werkzoekendeprofielen_bron.exception.WerkzoekendeNotFoundException;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.VumService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the VumController
 */
@ExtendWith(MockitoExtension.class)
class VumControllerTest {

    public static final String ID_WERKZOEKENDE = "ID_WERKZOEKENDE";
    public static final String X_VUMBERICHT_VERSIE = "X_VUMBERICHT_VERSIE";
    public static final String X_VUMFROM_PARTY = "X_VUMFROM_PARTY";
    public static final String X_VUMTO_PARTY = "X_VUMTO_PARTY";
    public static final String X_VUMVIA_PARTY = "X_VUMVIA_PARTY";
    public static final String X_FORWARDED_FOR = "subjectNumber=01234567890123456789";

       
    @Mock
    private VumService vumService;

    @Mock
    private ElkService elkService;
    
    @Mock
    private ConfigProperties properties;

    @InjectMocks
    private VumController vumController;

    private Boolean xVUMSUWIparty;

    @Mock
    private WerkzoekendeProfielMatchesRequest matchesRequest;
    @Mock
    private HttpServletRequest request;

    @Test
    void shouldGetWerkzoekendeProfiel() {

        Optional<Werkzoekende> werkzoekendeOptional = Optional.of(new Werkzoekende());
        
        when(properties.getVumProviderOin()).thenReturn("00000000001234567890123456789");
        when(vumService.findById(ID_WERKZOEKENDE, X_VUMTO_PARTY)).thenReturn(werkzoekendeOptional);

        ResponseEntity<Werkzoekende> werkzoekendeProfiel = vumController.getWerkzoekendeProfiel(ID_WERKZOEKENDE, X_VUMBERICHT_VERSIE, X_VUMFROM_PARTY, X_VUMTO_PARTY, xVUMSUWIparty, X_FORWARDED_FOR, X_VUMVIA_PARTY);

        String requestSignature = "GET /werkzoekendeProfielen/{idWerkzoekende}";
        String zoekopdracht_voor_matchingProfielen_werkzoekende = "Vraag detailprofielen op bij opgegeven idWerkzoekende";
        String requestPath = "/werkzoekendeProfielen/" + ID_WERKZOEKENDE;

        verify(elkService).handleRequest(werkzoekendeOptional.get(), X_VUMTO_PARTY, X_VUMFROM_PARTY, requestSignature, zoekopdracht_voor_matchingProfielen_werkzoekende, requestPath);
        verify(vumService).findById(ID_WERKZOEKENDE, X_VUMTO_PARTY);

        assertNotNull(werkzoekendeProfiel);
        assertEquals(werkzoekendeOptional.get(), werkzoekendeProfiel.getBody());
    }

    @Test
    void shouldNotGetWerkzoekendeProfiel() {

        Optional<Werkzoekende> werkzoekendeOptional = Optional.empty();
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.findById(ID_WERKZOEKENDE, X_VUMTO_PARTY)).thenReturn(werkzoekendeOptional);

        Assertions.assertThrows(WerkzoekendeNotFoundException.class, () -> vumController.getWerkzoekendeProfiel(ID_WERKZOEKENDE, X_VUMBERICHT_VERSIE, X_VUMFROM_PARTY, X_VUMTO_PARTY, xVUMSUWIparty, X_FORWARDED_FOR, X_VUMVIA_PARTY), "WerkzoekendeNotFoundException is verwacht");
    }

    @Test
    void shouldMatchWerkzoekendeProfielen() throws IOException {

        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> matches = new ImmutablePair<>(true, new WerkzoekendeMatchingProfielen(Collections.emptyList()));
        
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.match(matchesRequest, X_VUMTO_PARTY)).thenReturn(matches);

        vumController.matchesWerkzoekendeProfielen(X_VUMBERICHT_VERSIE, X_VUMFROM_PARTY, X_VUMTO_PARTY, xVUMSUWIparty, matchesRequest, X_FORWARDED_FOR, X_VUMVIA_PARTY, request);

        verify(elkService).handleRequest(matchesRequest, X_VUMTO_PARTY, X_VUMFROM_PARTY, "POST /werkzoekendeProfielen/matches", "Zoekopdracht voor MatchingProfielen Werkzoekende",
                "/werkzoekendeProfielen/matches");
        verify(vumService).match(matchesRequest, X_VUMTO_PARTY);
    }
    
    @Test
    void matchWerkzoekendeProfielen_MismatchCertWithOinVumProvider() {
    	
    	ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> matches = new ImmutablePair<>(true, new WerkzoekendeMatchingProfielen(Collections.emptyList()));
        Optional<Werkzoekende> werkzoekendeOptional = Optional.empty();
        when(properties.getVumProviderOin()).thenReturn("01234567890123450000");
        
        Assertions.assertThrows(MismatchCertWithOinVumProviderException.class, () -> vumController.getWerkzoekendeProfiel(ID_WERKZOEKENDE, X_VUMBERICHT_VERSIE, X_VUMFROM_PARTY, X_VUMTO_PARTY, xVUMSUWIparty, X_FORWARDED_FOR, X_VUMVIA_PARTY), "MismatchCertWithOinVumProviderException is verwacht");
    }
}