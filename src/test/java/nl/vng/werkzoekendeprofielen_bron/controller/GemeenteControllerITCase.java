package nl.vng.werkzoekendeprofielen_bron.controller;

import com.c4_soft.springaddons.security.oauth2.test.annotations.Claims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.StringClaim;
import com.c4_soft.springaddons.security.oauth2.test.annotations.WithMockJwtAuth;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.GemeenteService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// To make loadFile instance and make use of objectmapper.
@TestInstance(PER_CLASS)
@WebMvcTest(controllers = GemeenteController.class)
@ActiveProfiles("test")
@Slf4j
class GemeenteControllerITCase {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GemeenteService service;
    @MockBean
    private ElkService elkService;

    private List<Werkzoekende> werkzoekendeList;
    private String werkzoekendeListJson;

    private static final String OIN = "123456789";

    private static final String PATH = "./src/test/resources/";


    @BeforeAll
    void loadFile() {

        // get the werzoekende as an object from the json file.
        try {
            werkzoekendeList = objectMapper.readValue(new File(PATH, "test-user-list.json"), new TypeReference<List<Werkzoekende>>() {
            });
            // get the werkzoekende as a String for the Json request.
            werkzoekendeListJson = new String(Files.readAllBytes(Paths.get(PATH, "test-user-list.json")));
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @WithMockJwtAuth(authorities = { "ROLE_dummy" },claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = OIN))))
    void postValidWerkzoekendeListWithCorrectOIN() throws Exception {
        when(service.saveAll(anyList(), eq(OIN))).thenReturn(werkzoekendeList);

        mockMvc.perform(post("/werkzoekende/lijst/" + OIN).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(werkzoekendeListJson))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(werkzoekendeList)));
    }

    @Test
    @WithMockJwtAuth(authorities = { "ROLE_dummy" },claims = @OpenIdClaims(otherClaims = @Claims(stringClaims = @StringClaim(name = "oin", value = "WRONG OIN"))))
    void postValidWerkzoekendeListWithIncorrectOIN() throws Exception {
        mockMvc.perform(post("/werkzoekende/lijst/" + OIN).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(werkzoekendeListJson))
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"code\":\"403.01\",\"message\":\"Geen authorizatie\",\"details\":\"403 FORBIDDEN \\\"Invalide OIN\\\"\"}"));
    }

}




