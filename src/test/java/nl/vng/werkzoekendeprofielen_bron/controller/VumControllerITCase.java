package nl.vng.werkzoekendeprofielen_bron.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.InlineResponse200;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekende;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.VumService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


// To make loadFile instance and make use of objectmapper.
@TestInstance(PER_CLASS)
@WebMvcTest(controllers = VumController.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
class VumControllerITCase {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private VumService vumService;

    @MockBean
    private ElkService elkService;
    
    @MockBean
    private ConfigProperties properties;

    private static final String ID = "exampleid";
    private static final String OIN = "00000001821002198000";
    private static final String PATH = "./src/test/resources/";

    private static MPWerkzoekendeMatch mpWerkzoekendeMatch;
    private static WerkzoekendeProfielMatchesRequest werkzoekendeRequest;
    private static Werkzoekende werkzoekende;
    private static MPWerkzoekende mpWerkzoekende;
    private static WerkzoekendeMatchingProfielen werkzoekendeMatchingProfielen;


    @BeforeAll
    void loadFile() {
        try {
            werkzoekende = objectMapper.readValue(new File(PATH, "test-user.json"), Werkzoekende.class);

        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    void setUp() {
        mpWerkzoekendeMatch = new MPWerkzoekendeMatch();
        werkzoekendeMatchingProfielen = new WerkzoekendeMatchingProfielen();
        mpWerkzoekende = new MPWerkzoekende();
        werkzoekendeRequest = new WerkzoekendeProfielMatchesRequest();
    }


    @Test
    void getWerkzoekendeFound() throws Exception {
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.findById(ID, TestRequestFactory.X_VUM_TO_PARTY_OIN)).thenReturn(Optional.ofNullable(werkzoekende));

        mockMvc.perform(TestRequestFactory.getWithHeaders("/werkzoekendeProfielen/" + ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(werkzoekende)));
    }

    @Test
    void getWerkzoekendeNotFound() throws Exception {
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.findById(any(), any())).thenReturn(Optional.empty());

        mockMvc.perform(TestRequestFactory.getWithHeaders("/werkzoekendeProfielen/" + ID)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"code\":\"400.01\",\"message\":\"Ongeldige aanroep\",\"details\":\"ID niet gevonden\"}"));
    }

    @Test
    void matchWerkzoekendeProfielenFound() throws Exception {

        werkzoekendeRequest.setVraagObject(mpWerkzoekende);
        werkzoekendeMatchingProfielen.setMatches(List.of(mpWerkzoekendeMatch));
        
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");

        when(vumService.match(any(), eq(OIN))).thenReturn(ImmutablePair.of(false, werkzoekendeMatchingProfielen));

        InlineResponse200 response = new InlineResponse200(false, werkzoekendeMatchingProfielen);

        mockMvc.perform(TestRequestFactory.postWithHeaders("/werkzoekendeProfielen/matches")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(werkzoekendeRequest)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(response)));

    }

    @Test
    void matchWerkzoekendeProfielenEmpty() throws Exception {

        werkzoekendeRequest.setVraagObject(mpWerkzoekende);
        werkzoekendeMatchingProfielen.setMatches(List.of());
        
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.match(any(), eq(OIN))).thenReturn(ImmutablePair.of(false, werkzoekendeMatchingProfielen));

        InlineResponse200 response = new InlineResponse200(false, werkzoekendeMatchingProfielen);

        mockMvc.perform(TestRequestFactory.postWithHeaders("/werkzoekendeProfielen/matches")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(werkzoekendeRequest)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(response)));

    }


}