package nl.vng.werkzoekendeprofielen_bron.controller;


import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class TestRequestFactory {

    public static final String X_VUM_FROM_PARTY_OIN = "01234567890123456789";
    public static final String X_VUM_TO_PARTY_OIN = "00000001821002198000";

    public static MockHttpServletRequestBuilder getWithHeaders(String url) {
        return MockMvcRequestBuilders.get(url)
                .header("X-VUM-berichtVersie", 1)
                .header("X-VUM-fromParty", X_VUM_FROM_PARTY_OIN)
                .header("X-VUM-toParty", X_VUM_TO_PARTY_OIN)
                .header("X-VUM-viaParty", "00000001821002197000")
                .header("X-VUM-SUWIparty", true)
        		.header("X-Forwarded-For", "bb,serialNumber=01234567890123456789,OU=");

    }

    public static MockHttpServletRequestBuilder postWithHeaders(String url) {
        return MockMvcRequestBuilders.post(url)
                .header("X-VUM-berichtVersie", 1)
                .header("X-VUM-fromParty", X_VUM_FROM_PARTY_OIN)
                .header("X-VUM-toParty", "00000001821002198000")
                .header("X-VUM-viaParty", "00000001821002197000")
                .header("X-VUM-SUWIparty", true)
        		.header("X-Forwarded-For", "bb,serialNumber=01234567890123456789,OU=");
    }

}