package nl.vng.werkzoekendeprofielen_bron.repository;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.Arbeidsmarktkwalificatie;
import nl.vng.werkzoekendeprofielen_bron.entity.Beroepsnaam;
import nl.vng.werkzoekendeprofielen_bron.entity.Contractvorm;
import nl.vng.werkzoekendeprofielen_bron.entity.Cursus;
import nl.vng.werkzoekendeprofielen_bron.entity.Flexibiliteit;
import nl.vng.werkzoekendeprofielen_bron.entity.Gedragscompetentie;
import nl.vng.werkzoekendeprofielen_bron.entity.MPArbeidsmarktkwalificatie;
import nl.vng.werkzoekendeprofielen_bron.entity.MPCursus;
import nl.vng.werkzoekendeprofielen_bron.entity.MPOpleiding;
import nl.vng.werkzoekendeprofielen_bron.entity.MPVervoermiddel;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkervaring;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekende;
import nl.vng.werkzoekendeprofielen_bron.entity.Opleiding;
import nl.vng.werkzoekendeprofielen_bron.entity.Rijbewijs;
import nl.vng.werkzoekendeprofielen_bron.entity.SectorBeroepsEnBedrijfsleven;
import nl.vng.werkzoekendeprofielen_bron.entity.Taalbeheersing;
import nl.vng.werkzoekendeprofielen_bron.entity.Vakvaardigheid;
import nl.vng.werkzoekendeprofielen_bron.entity.Vervoermiddel;
import nl.vng.werkzoekendeprofielen_bron.entity.Voorkeursland;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkervaring;
import nl.vng.werkzoekendeprofielen_bron.entity.Werktijden;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.mapper.SimpleMapper;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.VumService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import(ConfigProperties.class)
//Obtain additional beans for integration test.
@ComponentScan(basePackageClasses = {VumService.class, SimpleMapper.class}, excludeFilters={
        @ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, value= ElkService.class)})
@ActiveProfiles("test")
class WerkzoekendeSpecificationsITCase {


    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VumService service;


    private static final String OIN = "123456789";
    private static final String ID = "someid";
    private static WerkzoekendeProfielMatchesRequest werkzoekendeRequest;
    private static MPWerkzoekende requestWerkzoekende;
    private static Werkzoekende werkzoekende;

    @BeforeEach
    void setUp() {

        werkzoekende = new Werkzoekende();
        werkzoekende.setArbeidsmarktkwalificatie(new Arbeidsmarktkwalificatie());
        requestWerkzoekende = new MPWerkzoekende();
        requestWerkzoekende.setArbeidsmarktkwalificatie(new MPArbeidsmarktkwalificatie());
        werkzoekendeRequest = new WerkzoekendeProfielMatchesRequest(requestWerkzoekende);

        // Set werkzoekende OIN to static OIN to test the specifications in isolation of OIN.
        werkzoekende.setOin(OIN);
        werkzoekende.setIdWerkzoekende(ID);
    }

    /**
     * Util method to persist and assert empty result.
     */
    private void assertEmpty() {
        entityManager.persistAndFlush(werkzoekende);
        // Find all werkzoekende with static OIN and modified request.
        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = service.match(werkzoekendeRequest, OIN);
        assertThat(result.getRight().getMatches()).isEmpty();
    }

    /**
     * Util method to persist and assert a match.
     */
    private void assertFound() {
        entityManager.persistAndFlush(werkzoekende);
        // Find all werkzoekende with static OIN and modified request.
        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = service.match(werkzoekendeRequest, OIN);
        assertThat(result.getRight().getMatches()).hasSize(1);
    }


    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#IndicatieArgumentsMatches")
    void IndicatieLdrRegistratieMatches(Integer indicatie1, Integer indicatie2) {
        werkzoekende.setIndicatieLdrRegistratie(indicatie1);
        requestWerkzoekende.setIndicatieLdrRegistratie(indicatie2);
        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#IndicatieArgumentsDoesNotMatch")
    void IndicatieLdrRegistratieDoesNotMatch(Integer indicatie1, Integer indicatie2) {
        werkzoekende.setIndicatieLdrRegistratie(indicatie1);
        requestWerkzoekende.setIndicatieLdrRegistratie(indicatie2);
        assertEmpty();
    }


    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#IndicatieArgumentsMatches")
    void IndicatieBeschikbaarheidsContractgegevensMatches(Integer indicatie1, Integer indicatie2) {
        werkzoekende.setIndicatieBeschikbaarheidContactgegevens(indicatie1);
        requestWerkzoekende.setIndicatieBeschikbaarheidContactgegevens(indicatie2);
        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#IndicatieArgumentsDoesNotMatch")
    void IndicatieBeschikbaarheidsContractgegevensDoesNotMatch(Integer indicatie1, Integer indicatie2) {
        werkzoekende.setIndicatieBeschikbaarheidContactgegevens(indicatie1);
        requestWerkzoekende.setIndicatieBeschikbaarheidContactgegevens(indicatie2);
        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#VoorkeurslandArgumentsMatches")
    void VoorkeurslandMatches(List<Voorkeursland> firstList, List<Voorkeursland> secondList) {
        werkzoekende.setVoorkeursland(firstList);
        requestWerkzoekende.setVoorkeursland(secondList);
        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#VoorkeurslandArgumentsDoesNotMatch")
    void VoorkeurslandDoesNotMatch(List<Voorkeursland> firstList, List<Voorkeursland> secondList) {
        werkzoekende.setVoorkeursland(firstList);
        requestWerkzoekende.setVoorkeursland(secondList);
        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#VervoersmiddelArgumentsMatches")
    void VervoermiddelMatches(Integer firstIndication, Integer secondIndication) {
        Vervoermiddel middel1 = new Vervoermiddel();
        middel1.setIndicatieBeschikbaarVoorUitvoeringWerk(firstIndication);
        middel1.setIndicatieBeschikbaarVoorWoonWerkverkeer(secondIndication);
        werkzoekende.setVervoermiddel(List.of(middel1));
        requestWerkzoekende.setVervoermiddel(List.of(middel1));

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#VervoersmiddelArgumentsDoesNotMatch")
    void VervoermiddelDoesNotMatch(List<Vervoermiddel> firstList, List<MPVervoermiddel> secondList) {
        werkzoekende.setVervoermiddel(firstList);
        requestWerkzoekende.setVervoermiddel(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#FlexibiliteitArgumentsMatches")
    void FlexibiliteitMatches(Flexibiliteit flex1, Flexibiliteit flex2) {
        werkzoekende.setFlexibiliteit(flex1);
        requestWerkzoekende.setFlexibiliteit(flex2);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#FlexibiliteitArgumentsDoesNotMatch")
    void FlexibiliteitDoesNotMatch(Flexibiliteit flex1, Flexibiliteit flex2) {
        werkzoekende.setFlexibiliteit(flex1);
        requestWerkzoekende.setFlexibiliteit(flex2);

        assertEmpty();

    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#WerktijdenArgumentsMatches")
    void WerktijdenMatches(Werktijden tijd1, Werktijden tijd2) {
        werkzoekende.setWerktijden(tijd1);
        requestWerkzoekende.setWerktijden(tijd2);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#WerktijdenArgumentsDoesNotMatch")
    void WerktijdenDoesNotMatch(Werktijden tijd1, Werktijden tijd2) {
        werkzoekende.setWerktijden(tijd1);
        requestWerkzoekende.setWerktijden(tijd2);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#ContractvormArgumentsMatches")
    void ContractvormMatches(List<Contractvorm> firstList, List<Contractvorm> secondList) {
        werkzoekende.setContractvorm(firstList);
        requestWerkzoekende.setContractvorm(secondList);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#ContractvormArgumentsDoesNotMatch")
    void ContractvormDoesNotMatch(List<Contractvorm> firstList, List<Contractvorm> secondList) {
        werkzoekende.setContractvorm(firstList);
        requestWerkzoekende.setContractvorm(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#codeWerkEnDenkniveauWerkzoekendeArgumentsMatches")
    void codeWerkEnDenkniveauWerkzoekendeMatches(String codeWerkzoekende, String codeRequest) {
        werkzoekende.getArbeidsmarktkwalificatie().setCodeWerkEnDenkniveauWerkzoekende(codeWerkzoekende);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setCodeWerkEnDenkniveauWerkzoekende(codeRequest);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#codeWerkEnDenkniveauWerkzoekendeArgumentsDoesNotMatch")
    void codeWerkEnDenkniveauWerkzoekendeDoesNotMatch(String codeWerkzoekende, String codeRequest) {
        werkzoekende.getArbeidsmarktkwalificatie().setCodeWerkEnDenkniveauWerkzoekende(codeWerkzoekende);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setCodeWerkEnDenkniveauWerkzoekende(codeRequest);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#VakvaardigheidArgumentsMatches")
    void VakvaardigheidMatches(List<Vakvaardigheid> firstList, List<Vakvaardigheid> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setVakvaardigheid(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setVakvaardigheid(secondList);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#VakvaardigheidArgumentsDoesNotMatch")
    void VakvaardigheidDoesNotMatch(List<Vakvaardigheid> firstList, List<Vakvaardigheid> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setVakvaardigheid(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setVakvaardigheid(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#TaalbeheersingArgumentsMatch")
    void TaalbeheersingMatches(List<Taalbeheersing> firstList, List<Taalbeheersing> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setTaalbeheersing(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setTaalbeheersing(secondList);

        assertFound();
    }


    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#TaalbeheersingArgumentsDoesNotMatch")
    void TaalbeheersingDoesNotMatch(List<Taalbeheersing> firstList, List<Taalbeheersing> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setTaalbeheersing(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setTaalbeheersing(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#GedragscompetentieArgumentsMatches")
    void GedragscompetentieMatches(List<Gedragscompetentie> firstList, List<Gedragscompetentie> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setGedragscompetentie(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setGedragscompetentie(secondList);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#GedragscompetentieArgumentsDoesNotMatch")
    void GedragscompetentieDoesNotMatch(List<Gedragscompetentie> firstList, List<Gedragscompetentie> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setGedragscompetentie(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setGedragscompetentie(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#RijbewijsArgumentsMatches")
    void RijbewijsMatches(List<Rijbewijs> firstList, List<Rijbewijs> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setRijbewijs(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setRijbewijs(secondList);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#RijbewijsArgumentsDoesNotMatch")
    void RijbewijsDoesNotMatch(List<Rijbewijs> firstList, List<Rijbewijs> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setRijbewijs(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setRijbewijs(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#CursusArgumentsMatches")
    void CursusMatches(List<Cursus> firstList, List<MPCursus> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setCursus(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setCursus(secondList);

        assertFound();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#CursusArgumentsDoesNotMatch")
    void CursusDoesNotMatch(List<Cursus> firstList, List<MPCursus> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setCursus(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setCursus(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#OpleidingArgumentsMatches")
    void OpleidingMatches(List<Opleiding> firstList, List<MPOpleiding> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setOpleiding(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setOpleiding(secondList);

        assertFound();
        cleanOpleiding(firstList);
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#OpleidingArgumentsDoesNotMatch")
    void OpleidingDoesNotMatch(List<Opleiding> firstList, List<MPOpleiding> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setOpleiding(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setOpleiding(secondList);

        assertEmpty();
        cleanOpleiding(firstList);
    }

    private void cleanOpleiding(List<Opleiding> firstList) {
        //clean id for next test
        if (firstList != null) {
            for (Opleiding opleiding : firstList) {
                opleiding.getOpleidingsnaam().setId(null);
            }
        }
    }

    @ParameterizedTest //
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#WerkervaringArgumentsMatches")
    void WerkervaringMatches(List<Werkervaring> firstList, List<MPWerkervaring> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setWerkervaring(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setWerkervaring(secondList);

        assertFound();
    }

    @ParameterizedTest //
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#WerkervaringArgumentsDoesNotMatch")
    void WerkervaringDoesNotMatch(List<Werkervaring> firstList, List<MPWerkervaring> secondList) {
        werkzoekende.getArbeidsmarktkwalificatie().setWerkervaring(firstList);
        requestWerkzoekende.getArbeidsmarktkwalificatie().setWerkervaring(secondList);

        assertEmpty();
    }

    @ParameterizedTest //
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#SectorArgumentsMatches")
    void SectorMatches(List<SectorBeroepsEnBedrijfsleven> firstList, List<SectorBeroepsEnBedrijfsleven> secondList) {
        werkzoekende.setSector(firstList);
        requestWerkzoekende.setSector(secondList);

        assertFound();
    }

    @ParameterizedTest //
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#SectorArgumentsDoesNotMatch")
    void SectorDoesNotMatch(List<SectorBeroepsEnBedrijfsleven> firstList, List<SectorBeroepsEnBedrijfsleven> secondList) {
        werkzoekende.setSector(firstList);
        requestWerkzoekende.setSector(secondList);

        assertEmpty();
    }

    @ParameterizedTest
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#beroepsnaamArgumentsMatches")
    void BeroepsnaamMatches(List<Beroepsnaam> firstList, List<Beroepsnaam> secondList) {
        werkzoekende.setBemiddelingsberoep(firstList);
        requestWerkzoekende.setBemiddelingsberoep(secondList);

        assertFound();
        cleanBeroep(firstList);
    }

    @ParameterizedTest // TODO: fix Beroepsnaam matching to eagerly
    @MethodSource("nl.vng.werkzoekendeprofielen_bron.repository.ArgumentsProvider#beroepsnaamArgumentsDoesNotMatch")
    void BeroepsnaamDoesNotMatch(List<Beroepsnaam> firstList, List<Beroepsnaam> secondList) {
        werkzoekende.setBemiddelingsberoep(firstList);
        requestWerkzoekende.setBemiddelingsberoep(secondList);

        assertEmpty();
        cleanBeroep(firstList);
    }

    private void cleanBeroep(List<Beroepsnaam> firstList) {
        //clean id for next test
        if (firstList != null) {
            for (Beroepsnaam beroepsnaam : firstList) {
                beroepsnaam.setId(null);
            }
        }
    }


}
