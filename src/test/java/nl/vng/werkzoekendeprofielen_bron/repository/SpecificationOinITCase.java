package nl.vng.werkzoekendeprofielen_bron.repository;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekende;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.mapper.SimpleMapper;
import nl.vng.werkzoekendeprofielen_bron.service.ElkService;
import nl.vng.werkzoekendeprofielen_bron.service.VumService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import(ConfigProperties.class)
//Obtain additional beans for integration test. Exclude ElkService as not required.
@ComponentScan(basePackageClasses = {VumService.class, SimpleMapper.class, RequestRepository.class}, excludeFilters={
        @ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, value= ElkService.class)})
@ActiveProfiles("test")
class SpecificationOinITCase {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VumService service;

    private static final String OIN = "123456789";
    private static final String differentOIN = "987654321";
    private static final String ID = "someid";


    private static WerkzoekendeProfielMatchesRequest werkzoekendeRequest;
    private static Werkzoekende werkzoekende;

    @BeforeEach
    void setUp() {
        werkzoekende = new Werkzoekende();
        werkzoekende.setIdWerkzoekende(ID);
        werkzoekende.setOin(OIN);
        MPWerkzoekende requestWerkzoekende = new MPWerkzoekende();
        werkzoekendeRequest = new WerkzoekendeProfielMatchesRequest(requestWerkzoekende);
    }

    /**
     * Util method to persist and assert empty result.
     */
    private void assertEmpty(String oin) {
        entityManager.persistAndFlush(werkzoekende);
        // Find all werkzoekende with static OIN and modified request.
        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = service.match(werkzoekendeRequest, oin);
        assertThat(result.getRight().getMatches()).isEmpty();
    }

    /**
     * Util method to persist and assert a match.
     */
    private void assertFound(String oin) {
        entityManager.persistAndFlush(werkzoekende);
        // Find all werkzoekende with static OIN and modified request.
        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = service.match(werkzoekendeRequest, oin);
        assertThat(result.getRight().getMatches()).hasSize(1);
    }

    @Test
    void OinNullDoesNotMatch() {
        assertEmpty(null);
    }

    @Test
    void equalOinMatches() {
        werkzoekende.setOin(OIN);
        assertFound(OIN);
    }

    @Test
    void differentOinDoesNotMatch() {
        werkzoekende.setOin(OIN);
        assertEmpty(differentOIN);
    }


}