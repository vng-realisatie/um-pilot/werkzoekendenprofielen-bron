package nl.vng.werkzoekendeprofielen_bron.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.env.Environment;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test de ElasticConfig set up
 */
class ElasticConfigTest {

    @Test
    void shouldProduceElasticsearchClient() {
        Environment env = Mockito.mock(Environment.class);
        Mockito.when(env.getProperty(ElasticConfig.ELASTICSEARCH_URL)).thenReturn("url");

        RestHighLevelClient restHighLevelClient = new ElasticConfig(env).elasticsearchClient();

        Mockito.verify(env).getProperty(ElasticConfig.ELASTICSEARCH_URL);

        assertNotNull(restHighLevelClient);
    }
}