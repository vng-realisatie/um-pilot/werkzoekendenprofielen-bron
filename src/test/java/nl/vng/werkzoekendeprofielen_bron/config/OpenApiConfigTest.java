package nl.vng.werkzoekendeprofielen_bron.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests the opApi config construction
 */
class OpenApiConfigTest {

    @Test
    void shouldProduceOpenApiImpl() {

        OpenAPI openAPI = new OpenApiConfig().customOpenAPI();

        assertNotNull(openAPI);
        assertEquals(1, openAPI.getServers().size());
        assertEquals(OpenApiConfig.TITLE, openAPI.getInfo().getTitle());
        assertEquals(OpenApiConfig.VERSION, openAPI.getInfo().getVersion());
        assertEquals(OpenApiConfig.URL, openAPI.getInfo().getContact().getUrl());
        assertEquals(OpenApiConfig.VNG, openAPI.getInfo().getContact().getName());
        assertEquals(OpenApiConfig.DESCRIPTION, openAPI.getInfo().getDescription());

        Server server = openAPI.getServers().iterator().next();
        assertEquals(OpenApiConfig.HTTPS_EXAMPLEURL_COM, server.getUrl());
    }
}