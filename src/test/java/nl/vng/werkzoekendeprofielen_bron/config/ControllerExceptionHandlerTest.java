package nl.vng.werkzoekendeprofielen_bron.config;

import nl.vng.werkzoekendeprofielen_bron.exception.WerkzoekendeNotFoundException;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchCertWithOinVumProviderException;
import nl.vng.werkzoekendeprofielen_bron.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.werkzoekendeprofielen_bron.util.Error;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the ControllerExceptionHandler
 */
@ExtendWith(MockitoExtension.class)
class ControllerExceptionHandlerTest {

    public static final String MESSAGE = "message";
    private ControllerExceptionHandler exceptionHandler;

    @BeforeEach
    void setUp() {
        exceptionHandler = new ControllerExceptionHandler();
    }

    @Test
    void shouldHandleMissingRequestHeaderException() {
        MissingRequestHeaderException missingRequestHeaderException = mock(MissingRequestHeaderException.class);
        when(missingRequestHeaderException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleMissingRequestHeaderException(missingRequestHeaderException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONVOLLEDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.ONVOLLEDIGE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleMismatchRequestWithOinBemiddelaarException() {

        MismatchRequestWithOinBemiddelaarException mismatchRequestWithOinBemiddelaarException = mock(MismatchRequestWithOinBemiddelaarException.class);
        when(mismatchRequestWithOinBemiddelaarException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleMismatchRequestWithOinBemiddelaarException(mismatchRequestWithOinBemiddelaarException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getRight(), error.getMessage());

    }
    
    @Test
    void shouldHandleMismatchCertWithOinVumProviderException() {

    	MismatchCertWithOinVumProviderException wrongOinException = new MismatchCertWithOinVumProviderException();
        Error error = exceptionHandler.handleMismatchCertWithOinVumProviderException(wrongOinException);

        assertEquals("403 FORBIDDEN \"Het OIN is niet gelijk aan het OIN van de VUM Voorziening (HTTPS Certificaat)\"", error.getDetails());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.NIET_GEAUTORISEERDE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleNumberFormatException() {

        NumberFormatException numberFormatException = mock(NumberFormatException.class);
        when(numberFormatException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleNumberFormatException(numberFormatException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleMethodArgumentTypeMismatchException() {

        MethodArgumentTypeMismatchException methodArgumentTypeMismatchException = mock(MethodArgumentTypeMismatchException.class);
        when(methodArgumentTypeMismatchException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleMethodArgumentTypeMismatchException(methodArgumentTypeMismatchException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleNoSuchElementFoundException() {

        WerkzoekendeNotFoundException werkzoekendeNotFoundException = mock(WerkzoekendeNotFoundException.class);
        when(werkzoekendeNotFoundException.getReason()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleNoSuchElementFoundException(werkzoekendeNotFoundException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getRight(), error.getMessage());

    }

    @Test
    void shouldHandleConstraintViolationException() {

        ConstraintViolationException constraintViolationException = mock(ConstraintViolationException.class);
        when(constraintViolationException.getLocalizedMessage()).thenReturn(MESSAGE);
        Error error = exceptionHandler.handleConstraintViolationException(constraintViolationException);

        assertEquals(MESSAGE, error.getDetails());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getLeft(), error.getCode());
        assertEquals(ControllerExceptionHandler.ONGELDIGE_AANROEP.getRight(), error.getMessage());

    }
}