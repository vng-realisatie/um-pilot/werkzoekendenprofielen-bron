package nl.vng.werkzoekendeprofielen_bron.serializer;

import nl.vng.werkzoekendeprofielen_bron.entity.Emailadres;
import com.fasterxml.jackson.core.JsonGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

@ExtendWith(MockitoExtension.class)
class ValueSerializerEmailTest {

    @Test
    public void shouldSerialize() throws IOException {

        ValueSerializerEmail serializerEmail = new ValueSerializerEmail();
        Emailadres emailadres = new Emailadres();
        emailadres.setEmailadres("test@vng.nl");

        JsonGenerator jsonGenerator = Mockito.mock(JsonGenerator.class);
        serializerEmail.serialize(emailadres, jsonGenerator, null);

        Mockito.verify(jsonGenerator).writeString(emailadres.getEmailadres());
    }
}