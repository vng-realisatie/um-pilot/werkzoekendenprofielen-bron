package nl.vng.werkzoekendeprofielen_bron.validator;

import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Indirectly, tests the OneOfIntValidator
 */
class WerkzoekendeValidatorTest {

    private Validator validator;

    @BeforeEach
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void isValid() {
        Werkzoekende werkzoekende = new Werkzoekende();
        werkzoekende.setIdWerkzoekende("1");

        validator.validate(werkzoekende);
        Set<ConstraintViolation<Werkzoekende>> validate = validator.validate(werkzoekende);
        assertTrue(validate.isEmpty());
    }

    @Test
    void missingIdIsNotValid() {
        Werkzoekende werkzoekende = new Werkzoekende();
        werkzoekende.setIdWerkzoekende("");

        validator.validate(werkzoekende);
        Set<ConstraintViolation<Werkzoekende>> validate = validator.validate(werkzoekende);
        assertFalse(validate.isEmpty());
    }
}