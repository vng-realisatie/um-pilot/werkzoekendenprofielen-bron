package nl.vng.werkzoekendeprofielen_bron.service;

import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.repository.WerkzoekendeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GemeenteServiceTest {

    @Mock
    private WerkzoekendeRepository repository;

    @InjectMocks
    private GemeenteService service;


    private Werkzoekende werkzoekende1;
    private Werkzoekende werkzoekende2;
    private List<Werkzoekende> werkzoekendeList;
    private static final String ID1 = "ID1";
    private static final String ID2 = "ID2";
    private static final String OIN = "123456789";


    @BeforeEach
    void setUp() {
        werkzoekende1 = new Werkzoekende();
        werkzoekende1.setIdWerkzoekende(ID1);
        werkzoekende2 = new Werkzoekende();
        werkzoekende2.setIdWerkzoekende(ID2);
        werkzoekendeList = List.of(werkzoekende1, werkzoekende2);
    }

    @Test
    void shouldSaveAll() {
        when(repository.saveAll(werkzoekendeList)).thenReturn(werkzoekendeList);

        List<Werkzoekende> result = service.saveAll(werkzoekendeList, OIN);

        assertThat(result).hasSameElementsAs(werkzoekendeList);
    }

    @Test
    void shoudFindAll() {

        when(repository.findAllByOin(OIN)).thenReturn(werkzoekendeList);

        List<Werkzoekende> result = service.findAll(OIN);

        assertThat(result).hasSameElementsAs(werkzoekendeList);
    }



}