package nl.vng.werkzoekendeprofielen_bron.service;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekende;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.mapper.SimpleMapper;
import nl.vng.werkzoekendeprofielen_bron.repository.RequestRepository;
import nl.vng.werkzoekendeprofielen_bron.repository.WerkzoekendeRepository;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VumShuffleTest {

    @Mock
    private WerkzoekendeRepository werkzoekendeRepository;

    @Mock
    private RequestRepository requestRepository;

    @Mock
    private SimpleMapper mapper;

    private ConfigProperties properties;

    private static final int MAX_AMOUNT = 3;
    private static final String OIN = "123456789";
    private static VumService service;
    private WerkzoekendeProfielMatchesRequest request;

    @BeforeEach
    void setUp() {
        properties = mock(ConfigProperties.class);
        when(properties.getMaxAmountResponse()).thenReturn(MAX_AMOUNT);
        service = new VumService(werkzoekendeRepository, mapper, properties);
        request = new WerkzoekendeProfielMatchesRequest(new MPWerkzoekende());
    }

    @Test
    void returnEqualListIfSmallerOrEqualToMaxAmount() {

        List<Werkzoekende> werkzoekendeListMaxAmount = getList(MAX_AMOUNT);
        when(mapper.werkzoekendeToMPWerkzoekendeMatch(any())).thenReturn(new MPWerkzoekendeMatch());

        when(werkzoekendeRepository.findAll(ArgumentMatchers.<Specification<Werkzoekende>>any())).thenReturn(werkzoekendeListMaxAmount);

        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = service.match(request, OIN);

        List<MPWerkzoekendeMatch> resultList = result.getRight().getMatches();

        assertThat(resultList.size()).isEqualTo(MAX_AMOUNT);

    }

    @Test
    void returnShuffledListIfGreaterThanMaxAmount() {

        List<Werkzoekende> werkzoekendeListMaxAmount = getList(MAX_AMOUNT + 1);
        List<Werkzoekende> originalList = new ArrayList<>(werkzoekendeListMaxAmount);

        when(mapper.werkzoekendeToMPWerkzoekendeMatch(any())).thenReturn(new MPWerkzoekendeMatch());

        when(werkzoekendeRepository.findAll(ArgumentMatchers.<Specification<Werkzoekende>>any())).thenReturn(werkzoekendeListMaxAmount);

        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = service.match(request, OIN);

        List<MPWerkzoekendeMatch> resultList = result.getRight().getMatches();

        List<String> list1 = originalList.stream().map(x -> x.getIdWerkzoekende()).collect(Collectors.toList());
        List<String> list2 = resultList.stream().map(x -> x.getIdWerkzoekende()).collect(Collectors.toList());


        // The list should be shuffled and limited to max amount.
        assertThat(list1).isNotEqualTo(list2);
        assertThat(list2).hasSize(MAX_AMOUNT);

    }

    /**
     * Utility function to get a list of Werkzoekende with n numOfElements.
     */
    List<Werkzoekende> getList(int numOfElements) {

        List<Werkzoekende> werkzoekendeList = IntStream.range(0, numOfElements)
                .mapToObj(x -> new Werkzoekende()) //
                .collect(Collectors.toList());

        // Add UUID ID to identify every entity.
        werkzoekendeList.forEach(x -> x.setIdWerkzoekende(UUID.randomUUID().toString()));

        return werkzoekendeList;
    }


}
