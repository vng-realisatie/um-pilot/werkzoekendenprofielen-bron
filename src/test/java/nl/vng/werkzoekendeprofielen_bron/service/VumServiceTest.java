package nl.vng.werkzoekendeprofielen_bron.service;

import nl.vng.werkzoekendeprofielen_bron.config.ConfigProperties;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeMatchingProfielen;
import nl.vng.werkzoekendeprofielen_bron.dto.WerkzoekendeProfielMatchesRequest;
import nl.vng.werkzoekendeprofielen_bron.entity.MPWerkzoekendeMatch;
import nl.vng.werkzoekendeprofielen_bron.entity.Werkzoekende;
import nl.vng.werkzoekendeprofielen_bron.mapper.SimpleMapper;
import nl.vng.werkzoekendeprofielen_bron.repository.WerkzoekendeRepository;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class VumServiceTest {
    private static final String ID1 = "ID1";
    private static final String OIN = "123456789";
    private static final int MAX_AMOUNT = 3;

    @Mock
    private WerkzoekendeRepository werkzoekendeRepository;

    @Mock
    private SimpleMapper mapper;

    private ConfigProperties properties;
    @InjectMocks
    private VumService vumService;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WerkzoekendeProfielMatchesRequest request;
    @Mock
    private MPWerkzoekendeMatch mpWerkzoekendeMatch;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Werkzoekende werkzoekende;
    @BeforeEach
    void setUp() {
        properties = mock(ConfigProperties.class);
        when(properties.getMaxAmountResponse()).thenReturn(MAX_AMOUNT);

        MockitoAnnotations.openMocks(this);
    }

    @Test
    void findByIdSuccess() {

        when(werkzoekendeRepository.findByIdWerkzoekendeAndOin(ID1, OIN)).thenReturn(Optional.of(werkzoekende));

        vumService.findById(ID1, OIN);

        verify(werkzoekendeRepository).findByIdWerkzoekendeAndOin(ID1, OIN);
    }

    @Test
    void findByIdFailure() {
        when(werkzoekendeRepository.findByIdWerkzoekendeAndOin(ID1, OIN)).thenReturn(Optional.empty());

        vumService.findById(ID1, OIN);

        verify(werkzoekendeRepository).findByIdWerkzoekendeAndOin(ID1, OIN);
    }

    @Test
    void matchesEmpty() {
        when(werkzoekendeRepository.findAll(ArgumentMatchers.<Specification<Werkzoekende>>any())).thenReturn(List.of());

        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = vumService.match(request, OIN);

        assertThat(result.getLeft()).isFalse();
        assertThat(result.getRight().getMatches()).isEmpty();

    }

    @Test
    void matchesOne() {
        when(werkzoekendeRepository.findAll(ArgumentMatchers.<Specification<Werkzoekende>>any())).thenReturn(List.of(werkzoekende));
        when(mapper.werkzoekendeToMPWerkzoekendeMatch(any())).thenReturn(mpWerkzoekendeMatch);

        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = vumService.match(request, OIN);
        assertThat(result.getLeft()).isFalse();
        assertThat(result.getRight().getMatches()).containsExactly(mpWerkzoekendeMatch);

    }

    @Test
    void matchesMaxAmount() {

        List<Werkzoekende> werkzoekendeList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT, werkzoekende));
        List<MPWerkzoekendeMatch> mPwerkzoekendeList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT, mpWerkzoekendeMatch));


        when(werkzoekendeRepository.findAll(ArgumentMatchers.<Specification<Werkzoekende>>any())).thenReturn(werkzoekendeList100);
        when(mapper.werkzoekendeToMPWerkzoekendeMatch(any())).thenReturn(mpWerkzoekendeMatch);

        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = vumService.match(request, OIN);
        assertThat(result.getLeft()).isTrue();
        assertThat(result.getRight().getMatches()).hasSameElementsAs(mPwerkzoekendeList100);
    }

    @Test
    void matchesMaxAmountMinusOne() {

        List<Werkzoekende> werkzoekendeList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT - 1, werkzoekende));
        List<MPWerkzoekendeMatch> mPwerkzoekendeList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT - 1, mpWerkzoekendeMatch));


        when(werkzoekendeRepository.findAll(ArgumentMatchers.<Specification<Werkzoekende>>any())).thenReturn(werkzoekendeList100);
        when(mapper.werkzoekendeToMPWerkzoekendeMatch(any())).thenReturn(mpWerkzoekendeMatch);

        ImmutablePair<Boolean, WerkzoekendeMatchingProfielen> result = vumService.match(request, OIN);
        assertThat(result.getLeft()).isFalse();
        assertThat(result.getRight().getMatches()).hasSameElementsAs(mPwerkzoekendeList100);
    }

    @Test
    void shouldShuffle() {

        Werkzoekende werkzoekende1 = mock(Werkzoekende.class);
        Werkzoekende werkzoekende2 = mock(Werkzoekende.class);

        List<Werkzoekende> werkzoekenden = Arrays.asList(werkzoekende, werkzoekende1, werkzoekende2);
        List<Werkzoekende> result = vumService.pickRandom(werkzoekenden, 2);
        assertEquals(2, result.size());
    }
}