package nl.vng.werkzoekendeprofielen_bron.service;

import nl.vng.werkzoekendeprofielen_bron.dto.ElkEntity;
import nl.vng.werkzoekendeprofielen_bron.repository.RequestRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests the ElkService
 */
@ExtendWith(MockitoExtension.class)
class ElkServiceTest {

    @Mock
    private RequestRepository requestRepository;

    @InjectMocks
    private ElkService elkService;

    @Captor
    private ArgumentCaptor<ElkEntity> captor;

    @Test
    void shouldHandleRequest() {
        elkService.handleRequest("request", "oinTo", "oinFrom", "sign", "desc", "path");

        Mockito.verify(requestRepository).save(captor.capture());

        ElkEntity elkEntity = captor.getValue();
        assertEquals("request", elkEntity.getRequest());
        assertEquals("oinTo", elkEntity.getToOin());
        assertEquals("oinFrom", elkEntity.getFromOin());
        assertEquals("sign", elkEntity.getRequestSignature());
        assertEquals("desc", elkEntity.getRequestDescription());
        assertEquals("path", elkEntity.getRequestPath());
    }
}