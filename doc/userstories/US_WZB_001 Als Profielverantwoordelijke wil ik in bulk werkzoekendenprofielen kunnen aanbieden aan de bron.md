# Feature : US_WZB_001 Als Profielverantwoordelijke wil ik in bulk werkzoekendenprofielen kunnen aanbieden aan de bron

versie 0.10

_Versionering_

| versie | datum         | opmerking                     |
|--------|---------------|-------------------------------|
| 0.10   | januari 2023  | Initiele opzet                |
| 0.10   | februari 2023 | Aanvulling acceptatiecriteria |
| 0.10   | maart 2023    | Definitief                    |

**Als** Profielverantwoordelijke  
**Wil ik** in bulk werkzoekendenprofielen kunnen aanbieden aan de bron
**Zodat ik** deze profielen kan gebruiken voor matchingsdoeleinden

### Functioneel
Dit is een ondersteunende User Story die beschrijft waaraan de API dient te voldoen. 

Wanneer een persoon verhuist kan de situatie dat deze in UM nog bij de oude gemeente geregistreerd staat. 
Dit mag een registratie voor die persoon in de nieuwe gemeente niet in de weg staan. 
O.a. hierom moet het mogelijk zijn dezelfde werkzoekende in verschillende gemeentes (OIN) te registreren.

### Technische Documentatie

In de implementatie worden de volgende response codes genoemd 
"200", description = "OK"
"400", description = "Bad request"
"401", description = "not authenticated"
"403", description = "not authorized"
"429", description = "Too Many Requests, limiet is overschreden voor deze uitvraag" 
"500", description = "Internal Server Error"
"503", description = "Service Unavailable"


#### Postconditie
Na een bewerking is altijd de log bijgewerkt

### Acceptatiecriteria

*Feature: Toevoegen werkzoekendeprofielen*  
**Gegeven** de Client is geauthoriseerd  
**En** er zijn voor het OIN geen werkzoekendeprofielen aanwezig in de bron  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** zijn de werkzoekendeprofielen opgenomen in de bron   

*Scenario: Vervangen werkzoekendeprofielen*  
**Gegeven** de Client is geauthoriseerd  
**En** er zijn voor het OIN werkzoekendeprofielen aanwezig in de bron  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** zijn de bestaande werkzoekendeprofielen verwijderd uit de bron   
**En** zijn de nieuwe werkzoekendeprofielen opgenomen in de bron   

*Feature: Toevoegen niet valide werkzoekendeprofiel*  
**Gegeven** de Client is geauthoriseerd  
**En** het werkzoekendeprofiel is niet aanwezig in de bron  
**Wanneer** een valide JSON die niet voldoet aan de Business Rules aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad Request)  
**En** is het werkzoekendeprofiel niet opgenomen in de bron   

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  
**En** is het werkzoekendeprofiel niet opgenomen in de bron  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad request)  
**En** is het werkzoekendeprofiel niet opgenomen in de bron   

*Scenario : registreren werkzoekende bij OIN*  
**Gegeven** een gebruiker voert werkzoekendeprofielen op   
**En** het werkzoekendeId komt niet voor in de database  
**Wanneer** de JSON enkel unieke werkzoekendeIds bevat  
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON in de database opgeslagen  

*Scenario : registreren werkzoekende bij verschillende OINs*  
**Gegeven** een gebruiker voert werkzoekendeprofielen op   
**En** het werkzoekendeId komt reeds voor in de database bij een ander OIN  
**Wanneer** de JSON enkel unieke werkzoekendeIds bevat  
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON in de database opgeslagen  

*Scenario : registreren dubbele werkzoekende bij hetzelfde OIN*  
**Gegeven** een gebruiker voert werkzoekendeprofielen op   
**Wanneer** de JSON een dubbel werkzoekendeId bevat  
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON niet in de database opgeslagen  

*Scenario : registreren werkzoekende zonder Id bij hetzelfde OIN*  
**Gegeven** een gebruiker voert vacatures op   
**Wanneer** de JSON een entiteit met een leeg werkzoekendeId bevat  
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON niet in de database opgeslagen 
