# Feature : US_WZB_003 Als Gebruiker wil ik de detailsgegevens van een werkzoekendenprofiel kunnen opvragen bij de bron

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2022 | Initiele opzet                        |

**Als** Gebruiker
**wil ik** de detailsgegevens van een werkzoekendenprofiel kunnen opvragen bij de bron
**zodat** de gegevens van de werkzoekende geraadpleegd kunnen worden

### Functioneel
Deze userstory ondersteunt de mogelijkheid een werkzoekende profiel op te kunnen vragen

### Technische Documentatie
Van deze operatie is geen yml beschikbaar  
End point : /werkzoekendeProfielen/{idWerkzoekende}

### Acceptatiecriteria

*Feature: opvragen bestaand werkzoekende profiel*
**Gegeven** de Gebruiker is geautoriseerd  
**Wanneer** een werkzoekende profiel wordt opgevraagd  
**En** het werkzoekende profiel is aanwezig in de bron  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** wordt het werkzoekende profiel in de response geleverd

*Scenario: opvragen niet bestaand werkzoekende profiel*
**Gegeven** de Gebruiker is geautoriseerd  
**Wanneer** een werkzoekende profiel wordt opgevraagd   
**En** het werkzoekende profiel is niet aanwezig in de bron  
**Dan** retourneert de applicatie een http status 400 (Bad Request)  
**En** wordt er geen response geretourneerd

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad request)    
