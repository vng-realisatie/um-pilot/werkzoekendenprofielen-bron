# Feature : US_WZB_002 Als Profielverantwoordelijke wil ik werkzoekendenprofielen kunnen opvragen bij de bron

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | december 2022 | Initiele opzet |
| 0.10   | maart 2023    | Definitief     |


**Als** Profielverantwoordelijke
**wil ik** werkzoekendenprofielen kunnen opvragen bij de bron
**zodat** de gegevens van de werkzoekende geraadpleegd kunnen worden

### Functioneel
Deze userstory ondersteunt de mogelijkheid werkzoekendeprofielen van een OIN op te kunnen vragen

### Technische Documentatie
End point : /werkzoekende/lijst/{oin}

### Acceptatiecriteria

*Feature: opvragen bestaand werkzoekende profiel*  
**Gegeven** de Gebruiker is geautoriseerd  
**Wanneer** de werkzoekendeprofielen van een OIN worden opgevraagd  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** wordt de profielen van dat OIN in de response geleverd

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad request)  
